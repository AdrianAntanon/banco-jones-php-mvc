<?php

function validateRegister()
{

    $message = '';
    $affirmativeMessage = 'OK';
    $confirmation = true;

    if (!validateName()) {
        $_POST['userName'] = '';
        $message .= "El nombre solo puede contener letras y espacios" . '</br>';
        $confirmation = false;
    }

    if (!validateSurame()) {
        $_POST['userSurname'] = '';
        $message .= "El apellido solo puede contener letras y espacios" . '</br>';
        $confirmation = false;
    }

    if(!validateAge()){
        $_POST['birthdate'] = '';
        $message .= "Tienes que ser mayor de edad" . '</br>';
        $confirmation = false;
    }

    if(!checkWordDNI()){
        $_POST['dni'] = '';
        $message .= "El DNI es incorrecto" . '</br>';
        $confirmation = false;
    }

    if (!validateGenre()){
        $_POST['genre'] = '';
        $message .= "El género solo puede ser hombre o mujer" . '</br>';
        $confirmation = false;
    }

    if(!validateMobilePhone()){
        $_POST['mobilePhone'] = '';
        $message .= "El teléfono es incorrecto, tiene que ser un número de móvil" . '</br>' ;
        $confirmation = false;
    }

    if(!validateEmail()){
        $_POST['email'] = '';
        $message .= "Introduce un email correcto" . '</br>';
        $confirmation = false;
    }

    if(!validatePassword()){
        $_POST['pass'] = '';
        $message .= "La contraseña debe contener mayúsculas, minúsculas, números, un carácter especial y más de 8 caracteres o no coinciden" . '</br>';
        $confirmation = false;
    }

    if($confirmation){
        return $affirmativeMessage;
    }

    return $message;
}

function validateName()
{
    if (ctype_alpha(str_replace(' ', '', $_POST['userName']))) {
        return true;
    }
    return false;
}

function validateSurame()
{
    if (ctype_alpha(str_replace(' ', '',$_POST['userSurname']))) {
        return true;
    }
    return false;
}

function validateAge()
{
    $userBirthdate = new DateTime($_POST['birthdate']);

    $currentDate = date("d-m-Y");
    $currentDate = new DateTime($currentDate);

    $interval = $userBirthdate->diff($currentDate);
    $myage = $interval->y;

    if ($myage >= 18) {
        return true;
    } else {
        return false;
    }
}

function checkWordDNI()
{
    if($_POST['dni']){
        $word = substr($_POST['dni'], -1);
        $numbers = substr($_POST['dni'], 0, -1);
        if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numbers%23, 1) == $word && strlen($word) == 1 && strlen ($numbers) == 8 ){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }

}

function validateGenre(){
    if($_POST['genre']){
        if($_POST['genre'] == "Selecciona tu género"){
            return false;
        }else{
            if($_POST['genre']== "Hombre"){
                $_POST['genre']=true;
            }else{
                $_POST['genre']=false;
            }
            return true;
        }
    }else{
        return false;
    }
}

function validateMobilePhone()
{
    $numbers = strlen($_POST['mobilePhone']);
    $firstNumber = substr($_POST['mobilePhone'], 0, 1);

    if ($numbers === 9) {
        if ($firstNumber === '6' || $firstNumber === '7') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function validateEmail(){
    $email = $_POST['email'];

    if($email){
        return (false !== filter_var($email, FILTER_VALIDATE_EMAIL));
    }else{
        return false;
    }
}

function validatePassword()
{

    $pass = $_POST['pass'];

    $repeatPass = $_POST['repeatPass'];

    if (strlen($pass) >= '8' && preg_match("#[0-9]+#", $pass) && preg_match("#[A-Z]+#", $pass) &&  preg_match("#[a-z]+#", $pass) && $pass === $repeatPass){
        return true;
    }

    return false;

}
