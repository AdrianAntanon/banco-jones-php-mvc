<?php


class Account
{
    private $account, $creation, $balance, $id;

    /**
     * Account constructor.
     * @param $account string
     * @param $creation DateTime
     * @param $balance double
     * @param $id integer
     */
    public function __construct($account, $balance, $id)
    {
        $this->account = $account;
        $this->balance = $balance;
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

        /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }



    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * @param DateTime $creation
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }





}